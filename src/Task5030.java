import java.util.Arrays;
public class Task5030{
    public static void main(String[] args) throws Exception {
        String input = "Devcamp";
        
        System.out.println(isString(input)); // true
        System.out.println(isString(1)); // false

        System.out.println("-----------------------------------------------");
        String catChuoi = "Robin Singh";
        int n = 4;
        String output = cutString(catChuoi, n);
        System.out.println(output);

        System.out.println("-----------------------------------------------");
        String dauVao = "Robin Singh";
        String[] dauRa = convertToWordArray(dauVao);
        
        // In ra mảng các từ trong chuỗi
        System.out.println(Arrays.toString(dauRa));
        

        System.out.println("-----------------------------------------------");
        String dauVao1 = "Robin Singh from USA";
        String dauRa1 = convertToFormat(dauVao1);
        System.out.println(dauRa1);

        System.out.println("-----------------------------------------------");
        String input1 = "JavaScript Exercises";
        String input2 = "JavaScript exercises";
        String input3 = "JavaScriptExercises";
        
        String output1 = processString(input1);
        String output2 = processString(input2);
        String output3 = processString(input3);
        
        System.out.println(output1);
        System.out.println(output2);
        System.out.println(output3);

        System.out.println("-----------------------------------------------");
        String input4 = "js string exercises";
        String output4 = capitalizeFirstLetter(input4);
        System.out.println(output4);

        System.out.println("-----------------------------------------------");
        repeatWord("Ha!", 1); // "Ha!"
        repeatWord("Ha!", 2); // "Ha! Ha!"
        repeatWord("Ha!", 3); // "Ha! Ha! Ha!"

        System.out.println("-----------------------------------------------");
        String input5 = "dcresource";
        int n1 = 2;
        String[] result1 = splitString(input5, n1);
        System.out.println(Arrays.toString(result1));

        String input6 = "dcresource";
        int n2 = 3;
        String[] result2 = splitString(input6, n2);
        System.out.println(Arrays.toString(result2));

        System.out.println("-----------------------------------------------");
        String str = "The quick brown fox jumps over the lazy dog";
        String subStr = "the";

        int count = countSubstringOccurrences(str, subStr);
        System.out.println("Output: " + count);

        System.out.println("-----------------------------------------------");
        String input7 = " 0000";
        String replacement = "123";
        int n7 = 4;

        String output7 = replaceRightCharacters(input7, replacement, n7);
        System.out.println(output7);
    }

    //phương thức kiểm tra giá trị truyền vào có phải là chuỗi hay không 
    public static boolean isString(Object value) {
        return value instanceof String;
    }

    //phương thức Cắt n phần tử đầu tiên của chuỗi
    public static String cutString(String input, int n) {
        if (input.length() <= n) {
            return input;
        } else {
            return input.substring(0, n);
        }
    }

    //phương thức Chuyển chuỗi thành mảng các từ trong chuỗi
    public static String[] convertToWordArray(String input) {
        return input.split(" ");
    }

    //Chuyển chuỗi về định dạng như output "robin-singh-from-usa"
    public static String convertToFormat(String input) {
        // Chuyển đổi chuỗi sang chữ thường
        String lowercase = input.toLowerCase();
        
        // Thay thế khoảng trắng bằng dấu "-"
        String replaced = lowercase.replace(" ", "-");
        
        return replaced;
    }

    //Bỏ các khoảng trắng giữa các từ trong chuỗi, viết hoa chữ cái đầu tiên của mỗi từ
    public static String processString(String input) {
        String[] words = input.split(" "); // Tách các từ trong chuỗi
        
        StringBuilder result = new StringBuilder();
        for (String word : words) {
            if (!word.isEmpty()) {
                char firstChar = Character.toUpperCase(word.charAt(0)); // Viết hoa chữ cái đầu tiên
                String restOfWord = word.substring(1); // Lấy phần còn lại của từ
                result.append(firstChar).append(restOfWord);
            }
        }
        
        return result.toString();
    }


    //phương thức viết hoa chữ cái đầu tiên của mỗi từ 
    public static String capitalizeFirstLetter(String input) {
        StringBuilder sb = new StringBuilder();

        // Chuyển chuỗi thành mảng các từ
        String[] words = input.split(" ");

        // Với mỗi từ trong mảng, viết hoa chữ cái đầu tiên và thêm vào StringBuilder
        for (String word : words) {
            if (word.length() > 0) {
                char firstLetter = Character.toUpperCase(word.charAt(0));
                sb.append(firstLetter).append(word.substring(1)).append(" ");
            }
        }

        // Xóa dấu cách cuối cùng (nếu có) và trả về kết quả
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    //Lặp lại n lần từ 1 từ cho trước, có khoảng trắng giữa các lần lặp
    public static void repeatWord(String word, int n) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < n; i++) {
            sb.append(word);
            if (i < n - 1) {
                sb.append(" ");
            }
        }

        String result = sb.toString();
        System.out.println(result);
    }

    //Đưa chuỗi thành mảng mà mỗi phần tử là n ký tự lần lượt từ chuỗi đó
    public static String[] splitString(String input, int n) {
        int length = input.length();
        int numSubstrings = length / n;
        String[] substrings = new String[numSubstrings];

        for (int i = 0; i < numSubstrings; i++) {
            int startIndex = i * n;
            int endIndex = startIndex + n;
            substrings[i] = input.substring(startIndex, endIndex);
        }

        return substrings;
    }

    //Đếm số lần xuất hiện của chuỗi con trong 1 chuỗi cho trước
    public static int countSubstringOccurrences(String str, String subStr) {
        int count = 0;
        int lastIndex = 0;

        while (lastIndex != -1) {
            lastIndex = str.toLowerCase().indexOf(subStr.toLowerCase(), lastIndex);

            if (lastIndex != -1) {
                count++;
                lastIndex += subStr.length();
            }
        }

        return count;
    }

    //Thay n ký tự bên phải của một chuỗi bởi 1 chuỗi khác
    public static String replaceRightCharacters(String input, String replacement, int n) {
        if (n >= input.trim().length()) {
            return replacement;
        }

        int startIndex = input.trim().length() - n;
        String substring = input.substring(0, startIndex);
        return substring + replacement + input.substring(startIndex + replacement.length());
    }
}
